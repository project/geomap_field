# Geomap Field

Provide a new field type to store address (using geolocation and map visualization). Also provide a field formatter to display the field as a map.