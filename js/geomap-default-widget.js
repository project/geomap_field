/**
 * @file
 * Geomap default widget custom behavior
 */

(function ($, Drupal) {
  Drupal.behaviors.geomapDefaultWidget = {
    attach(context) {
      $(once('geomapDefaultWidget', '.geomap-widget-map', context)).on(
        'map:afterInit',
        function (event, map) {
          const $map = $(event.target);
          const geolocationPlugin = $map.data('geolocation-plugin');
          const reverseUrl = `/geolocation_provider/reverse/${geolocationPlugin}`;
          const geolocationUrl = `/geolocation_provider/geolocation/${geolocationPlugin}`;
          const $widget = $map.parents('.geomap-widget-wrapper');
          const $latlon = $widget.find('.latlon-wrapper');
          const $latField = $latlon.find('.geomap-field-lat');
          const $lonField = $latlon.find('.geomap-field-lon');

          const marker = new L.marker(
            [$latField[0].value || 51.505, $lonField[0].value || -0.09],
            { draggable: 'true' },
          );
          marker.on(
            'dragend',
            function (event) {
              const position = marker.getLatLng();
              $latField[0].value = position.lat;
              $lonField[0].value = position.lng;
              this.updateLoader($widget, true);
              $.get(
                `${reverseUrl}/${position.lat}/${position.lng}`,
                function (data) {
                  this.updateSuggestions($widget, data);
                  this.updateLoader($widget, false);
                  if (data.features && data.features.length > 0) {
                    this.updateFeatureInput($widget, data.features[0]);
                  }
                }.bind(this),
              );
            }.bind(this),
          );
          map.addLayer(marker);
          $widget.marker = marker;

          // Listen Lat/Lon fields changes.
          $latlon.find('input').on(
            'input',
            Drupal.debounce(
              function (event) {
                this.updateMarker($widget, map);
              }.bind(this),
              200,
            ),
          );

          // Geolocation behavior.
          $widget.find('.geomap-geolocation-btn').on(
            'click',
            function (event) {
              const street = $widget.find('input[name$="][street]"]')[0].value;
              const zipcode = $widget.find('input[name$="][zipcode]"]')[0]
                .value;
              const city = $widget.find('input[name$="][city]"]')[0].value;
              const country = $widget.find('input[name$="][country]"]')[0]
                .value;
              const address = `${street}, ${zipcode} ${city}, ${country}`;
              this.updateLoader($widget, true);
              $.get(
                `${geolocationUrl}/${address}`,
                function (data) {
                  this.updateLoader($widget, false);
                  if (data.features && data.features.length > 0) {
                    const feature = data.features[0];
                    $latField[0].value = feature.geometry.coordinates[1];
                    $lonField[0].value = feature.geometry.coordinates[0];
                    this.updateMarker($widget, map);
                    this.updateFeatureInput($widget, feature);
                  }
                }.bind(this),
              );
              event.preventDefault();
            }.bind(this),
          );
        }.bind(this),
      );
    },

    updateMarker($widget, map) {
      const lat = $widget.find('.geomap-field-lat')[0].value;
      const lon = $widget.find('.geomap-field-lon')[0].value;
      if (lat && lon) {
        const latlon = new L.LatLng(lat, lon);
        if (latlon !== $widget.marker.getLatLng()) {
          $widget.marker.setLatLng(latlon);
          map.setView(latlon);
        }
      }
    },

    updateLoader($widget, status) {
      $widget
        .find('.geomap-loader')
        .attr('aria-hidden', status ? 'false' : 'true');
    },

    updateSuggestions($widget, featureCollection) {
      const $list = $widget.find('.geolocation-suggestions-list');
      $list.empty();
      const features = featureCollection.features || [];
      features.forEach(
        function (feature) {
          const $item = $('<li>', {
            class: 'geolocation-suggestions-list-item',
          });
          const $btn = $('<button>');
          $btn.data('feature', feature);
          $btn.on('click', this.onSuggestionClick.bind(this));
          $btn.get(0).textContent = feature.properties.label;
          $list.append($item.append($btn));
        }.bind(this),
      );
    },

    onSuggestionClick(event) {
      const $suggestion = $(event.target);
      const feature = $suggestion.data('feature');
      // Update address input fields.
      const $widget = $suggestion.parents('.geomap-widget-wrapper');
      $widget.find('input[name$="][street]"]')[0].value =
        feature.properties.name;
      $widget.find('input[name$="][zipcode]"]')[0].value =
        feature.properties.postcode;
      $widget.find('input[name$="][city]"]')[0].value = feature.properties.city;
      this.updateFeatureInput($widget, feature);
      event.preventDefault();
    },

    updateFeatureInput($widget, feature) {
      $widget.find('input[name$="][feature]"]')[0].value =
        JSON.stringify(feature);
    },
  };
})(jQuery, Drupal);
